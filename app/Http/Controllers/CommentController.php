<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PhpParser\Comment;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = DB::table('comment')->get();
        // dd($comments);
        return view('comment')->with('comments', $comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'comment' => 'required|min:1'
        ]);

        DB::table('comment')->insert([
            'id' => $request->uid,
            'pid' => $request->pid,
            'detail' => $request->comment,
            'star' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        
        $request->session()->flash('message', 'Comment Success');
        //$request->session()->get('message','somment sucsses')
        return redirect('post/' . $request->pid);
        //return redirect('post/' . $request->pid)->with('session', $session);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($pid)
    {
        $comment = DB::table('comment')->where('pid', '=', $pid) ->get();
        // dd($comment);
        return view('comment')->with('comment', $comment);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cid)
    {
        DB::table('comment')
       ->where('cid','=',$cid)
       ->update([
           'detail' => $request->editdetail,
           'star' =>$request->star,
            'updated_at' => now() 
       ]);
       return redirect('post/' . $request->pid);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$cid)
    {
        DB::table('comment')->where('cid','=',$cid)->delete();
        return redirect('post/' . $request->pid);
    }
}
