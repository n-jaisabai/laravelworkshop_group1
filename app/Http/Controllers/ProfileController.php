<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;

session_start();

use Illuminate\Http\Request;
use DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = DB::table('users')->get();
        return view('profile')->with('profile', $profile);
        //echo "Index";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        echo "Create";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        echo "Story";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = DB::table('users')->where('id', '=', $id)->get();
        $mypost = DB::table('post')->leftJoin('users', 'users.id', '=', 'post.id')->where('users.id', '=', $id)->get();
        return view('auth/profile')->with('mypost', $mypost)->with('profile', $profile);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('users')->find($id); //หาเฉพาะ id
        //dd($data);
        return view('auth/editProfile')->with('edit', $data);
        //echo "Edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //echo  $id.'<br>'.$request->name.'<br>'.$request->email.'<br>'.Hash::make($request->password).'<br>'.now();
        DB::table('users')->where("id", "=", $id)->update([
            'id' => $id,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'updated_at' => now()
        ]);
        //dd($data);
        return redirect('profile/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
