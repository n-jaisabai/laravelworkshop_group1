<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = DB::table('post')->get();
        $posts = DB::table('post')->leftJoin('users', 'post.id', '=', 'users.id')->orderby('like', 'DESC')->get();
        // dd($posts);
        return view('post')->with('posts', $posts)->with('date', $date);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createPost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|min:10|max:100',
            'description' => 'required|min:5'
        ]);

        DB::table('post')->insert([
            'id' => $request->uid,
            'pname' => $request->title,
            'detail' => $request->description,
            'like' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        $request->session()->flash('message', 'Create Success');
        return redirect('post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $pid
     * @return \Illuminate\Http\Response
     */
    public function show($pid)
    {
        $post = DB::table('post')->where('pid', '=', $pid)->get();
        $comment = DB::table('comment')->leftJoin('users', 'users.id', '=', 'comment.id')->where('pid', '=', $pid)->orderby('star', 'DESC')->get();
        $dateComment = DB::table('comment')->where('pid', '=', $pid)->get();
        $like = DB::table('like_status')->where('p_id', '=', $pid)->get();

        $like_user = [];
        foreach ($like as $key => $value) {
            array_push($like_user, $value->u_id);
        }


        // dd($like_user);
        return view('postDetail')->with('post', $post[0])->with('comment', $comment)->with('dateComment', $dateComment)->with('like', $like)->with('like_user', $like_user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $pid)
    {
        $post = DB::table('post')->where('pid', '=', $pid)->get();
        //dd($post);
        return view('editPost')->with('post', $post[0]);
        return redirect($request->path);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $pid)
    {
        DB::table('post')
            ->where('pid', '=', $pid)
            ->update([
                'pname' => $request->pname,
                'like' => $request->like,
                'detail' => $request->detail,
                'updated_at' => now()
            ]);
        return redirect($request->path);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $pid)
    {
        DB::table('post')->where('pid', '=', $pid)->delete();
        return redirect($request->path);
    }
}
