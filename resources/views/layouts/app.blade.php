<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>MASOGAN | @yield('title', 'Home')</title>
    {{-- Bootstarp --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://kit.fontawesome.com/1d062fe15b.js"></script>
    <script rel="stylesheet" src="{{ asset('css/post.css') }}"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
    
    {{-- FontLOGO --}}
    {{--  <link href="https://fonts.googleapis.com/css?family=Libre+Caslon+Display&display=swap" rel="stylesheet">  --}}
    <link href="https://fonts.googleapis.com/css?family=Athiti&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster&display=swap" rel="stylesheet">
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/post.css') }}" rel="stylesheet">
</head>
<body>

    @section('title', 'Home')
        
    @if(Auth::user())
        {{ Session::flash('id', Auth::user()->id) }}
    @endif
    
<div id="app">
<body style="background-color: rgb(113, 183, 144)">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container ">
                <a class="navbar-brand" href="{{ url('/home') }}">
                    <img src="{{asset('img/LOGOpost.png')}}" class="img-fluid" alt="Responsive image" style="width: 60px; height: 60px;" >
                </a>
                <h4 class="fontlogo mt-2">MASOGAN</h4>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        
                           
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <div class="container">
                                <div class="row ">
                                    <div class="col-sm4 mt-3 fonttext " >
                                    
                                        <a href="{{ url('post/create') }}"  style="color: rgb(36, 158, 107);">
                                            <div class="col-sm12 button-group">
                                                <button  type="button" class="btn text-white font-weight-bold " style="background-color: rgb(36, 158, 107);">  
                                                    <i style="font-size:20px " class="fas fa-plus-square"></i>
                                                    <label>NEW POSTS</label>
                                                </button> 
                                            </div>
                                        </a>  
                                   
                                    </div>
                                   
                                    <div class="col-sm8 ">
                                        <li class="nav-item dropdown ">
                                            <a id="navbarDropdown dropdown-toggle" class="nav-link " href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                <div class="">
                                                    <img src="https://scontent.fbkk5-5.fna.fbcdn.net/v/t1.0-9/16708402_1649221241761213_8283960341434495948_n.jpg?_nc_cat=100&_nc_eui2=AeEfKGl6pkfmdIuPaBeBiZBxStRWowhDnYF4RBTrCh_ru5vUunw_iJJ8Wb0kES_EgNPAf9QxFrTUv1P5LXYR0G-aoKvKMp5yULE9cS5gGJ8QXA&_nc_oc=AQlcJwBONHk4h9jXpe8DMpZ06cBwJbuomnSpYUghOmad6nVy_vYi3xk2E_VSNJ14BB0&_nc_ht=scontent.fbkk5-5.fna&oh=720e3d42315a18e59e720dabca93a967&oe=5DDC8668" class="img-fluid rounded-circle" alt="Responsive image" style="width: 60px; height: 60px;" >
                                                    {{ Auth::user()->name }} <span class="caret"></span>
                                                </div>
                                            </a>

                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                               
                                                <a class="dropdown-item" href="{{ url('profile/'.Auth::user()->id ) }} "><i style="font-size:24px;color:rgb(86, 194, 113);" class="fas fa-address-card"></i>
                                                    My Profile</a>

                                                <a class="dropdown-item" href="{{ url('profile/'.Auth::user()->id.'/edit' ) }} ">
                                                    
                                                    <i style="font-size:24px;color:rgb(86, 194, 113);" class="fas fa-address-card"></i>
                                                    Edit Profile</a>
                                               
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();"><i style="font-size:24px;color:rgb(224, 60, 49);" class="fa">&#xf146;</i>  
                                                    {{ __('Logout') }}
                                                </a >
                                               
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                                
                                            </div>
                                        </li>
                                    </div>
                            </div>
                        </div>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4 container ">
            @yield('content')
        </main>
    </div>
</body>
</html>
<style>
    .fontlogo{
        font-family: 'Libre Caslon Display', serif;
    }
    .fonttext{
        font-family: 'Athiti', sans-serif;
    }
    .fontwrite{
        font-family: 'Lobster', cursive;
    }
   
</style>