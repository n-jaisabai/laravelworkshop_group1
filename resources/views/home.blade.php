@extends('layouts.app')

@section('content')
<div class="container">
    @foreach ($post as $p)  
    <a href="{{ '/post/' . $p->pid }}"> 
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <b>{{ $p->pname }}</b> <br>                    
                    {{ $p->detail }} <br>
                    <i class="fa fa-user-circle-o"></i> {{ $p->name }} &ensp;
                    <i class="fa fa-clock-o"></i> {{ $p->updated_at }} &ensp;
                    <i class="fa fa-heart"></i> {{ $p->like }} &ensp;
                </div>
            </div>
        </div>
    </div>
    </a>
    @endforeach
</div>
@endsection
