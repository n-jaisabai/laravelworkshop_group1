<div class="card my-4 mr-5">
        <h5 class="card-header" style="text-align: left">Comment:</h5>
        <div class="card-body">
          <form action="{{ url('comment') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
              <textarea class="form-control" name="comment" rows="3"></textarea>
            </div>
            <input type="hidden" name="uid" value="{{ $user_id}}">
            <input type="hidden" name="pid" value="{{ $post->pid }}">
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
</div>