<div class="card mr-5 ml-5">
    <h5 class="card-header">Comment:</h5>
    <div class="card-body">
      <form action="{{url('comment',[$c->cid])}}" method="POST">
          @csrf
          @method('PUT')
        {{ csrf_field() }}
        <div class="form-group">
          <textarea class="form-control" name="editdetail" rows="3">{{ $c->detail }}</textarea>
        </div>
        <input type="hidden" name="pid" value="{{ $post->pid }}">
        <input type="hidden" name="star" value="{{ $c->star }}">
        <button type="submit" class="btn btn-primary pull-right">Submit</button>
      </form>
    </div>
</div>