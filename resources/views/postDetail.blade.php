@extends('layouts/app')

@section('title', $post->pname)

@section('content')

    <?php 
        if (Auth::user()) {
            $user_id = Auth::user()->id;
        } else {
            $user_id = -1;
        }
    ?>

    <div class="card" style="padding: 2%">
        <div>
            @if ( $user_id == $post->id) 
                            <div class="dropdown dropleft ">
                                    <button class="btn float-right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons ">more_horiz</i>
                                    </button>
                                    <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{ $post->pid . '/edit'}}"><i class="material-icons">edit</i>Edit</a>
                                        <form action="{{ url('post', [$post->pid]) }}" method="POST" >
                                            @csrf
                                            @method('DELETE')
                                            {{ csrf_field() }}
                                            <input type="hidden" name="path" value="post">
                                            <button class="dropdown-item" class="dropdown-item" onclick="return confirm('Are you sure want to delete?');" type="submit"><i class="material-icons">delete</i>Delete</button>
                                      </form>
                                     
                                    </div>
                            </div>
            @endif
        <h4><strong> {{ $post->pname }} </strong></h4>
    </div>
    
        <p>{{ $post->detail }}</p>
        
        <p class="text-secondary"><i class="fa fa-clock-o"></i> {{$post->created_at}} 
            ( แก้ไขล่าสุด  {{$post->updated_at}} )
        </p>
        @if (Auth::user())
        <div class="row">
            <div class="col">
            <form action="{{ url('like',[$post->pid]) }}" method="POST">
                    @csrf
                    @method('PUT')
                    {{ csrf_field() }}
                <input type="hidden" value="{{ $post->pname }}" name="pname">
                <input type="hidden" value="{{ $post->detail }}" name="detail">
                <input type="hidden" value="{{ 'post/'.$post->pid }}" name="path">
                <input type="hidden" value="{{ $user_id }}" name="uid">
                
                <?php $l = $post->like ?>

                @if (in_array($user_id, $like_user))
                    <input type="hidden" value="1" name="like_status">

                    <?php if ($post->like == 0) {
                        $post->like = 0;
                    } else {
                        $post->like -= 1;
                    } ?>
                    <button type="submit" class="btn btn-danger btn-circle btn-sm" value="{{ $post->like }}" name="like"><i class="fa fa-heart faa-pulse animated" ></i></button>
                     {{-- <button type="submit" class="btn "  value="{{ $post->like+=1 }}" name="like"><i class="fas fa-heart" style="font-size: 25px; color: rgb(186, 12, 47)"></i></button> --}}
                @else
                    <input type="hidden" value="0" name="like_status">
                    <button type="submit" class="btn btn-dark  btn-circle btn-sm" value="{{ $post->like+=1 }}" name="like"><i class="fa fa-heart faa-pulse animated" ></i></button>
                @endif
                <span class="text-secondary">{{$l}}</span>
            </form>
            </div>
            
        
        
            <div style="text-align: right;" class="col-11">
                <button type="button" data-toggle="collapse" data-target="#comment" class="btn btn-info" style="color:#ffffff">Comment</button>
                <div id="comment" class="collapse">
                    @include('addComment')  
                </div>
            </div>
        @endif 
    </div>  
    </div>
    </div>
    <br>
    <div>
         @include('comment')
    <div>
</div>
@endsection