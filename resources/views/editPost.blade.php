@extends('layouts/app')
@section('title', 'Edit post')
@section('content')

<?php 
  if (Auth::user() && Auth::user()->id == $post->id) {
    $user_id = Auth::user()->id;
  } else {
    header("Location: /home");
    die();
  }
?>

<form action="{{ url('post',[$post->pid]) }}" method="POST">
    @csrf
    @method('PUT')
    {{ csrf_field() }}
    <div class="card text-center " >
    <div class="form-group">
        <div class="card-header border rounded border-success" style="background-color: rgb(0, 149, 122)" >
      <h3 for="title " class="card-title fontlogo text-light font-weight-bold">Title</h3>
        </div>
        <div class="card-body">
        <input type="text" class="form-control" id="title" name="pname" placeholder="ชื่อกระทู้" value="{{ $post->pname }}">
        </div>
    </div>

    <div class="form-group">
        <div class="card-header border border-5 rounded border-success" style="background-color: rgb(0, 149, 122)">
      <h3 for="description" class="card-title text-light font-weight-bold">Description</h3>
        </div>
        <div class="card-body">
        <textarea class="form-control" id="description" name="detail" rows="5">{{ $post->detail }}</textarea>
        </div>
    </div>
    <input type="hidden" name="like" value="{{ $post->like }} ">
    <input type="hidden" value="{{ 'post/'.$post->pid }}" name="path">
  <input type="hidden" name="uid" value="{{ Session::get('id') }} ">
  <div class="card-body ">
    <button type="submit" class="btn  btn-block fontlogo text-light font-weight-bold" style="background-color: rgb(0, 149, 122)">Submit</button>
  </div>

    @if ($errors->any())
        <div class="alert alert-danger mt-3">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
     @endif

  </form>
@endsection
