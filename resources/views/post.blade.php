@extends('layouts.app')

@section('content')

    <?php 
        if (Auth::user()) {
            $user_id = Auth::user()->id;
        } else {
            $user_id = -1;
        }
    ?>

    @if (Session::has('message'))
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('message') }}
    </div>
    @endif
    <div class="container">
        <h3 class="fontlogo text-center text-white">ALL POSTS</h3>
    </div>
    <div class="container">
        <?php $i=0 ?> 
        @foreach ($posts as $post)
        <div class="row justify-content-center" style="margin: 1%">
            <div class="col-sm-12">
                <div class="card">
                       
                    <div class="card-body">
                            @if ( $user_id == $post->id) 
                                <div class="dropdown dropleft ">
                                        <button class="btn float-right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons ">more_horiz</i>
                                        </button>
                                        <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{'post/' . $post->pid . '/edit'}}"><i class="material-icons">edit</i>Edit</a>
                                            <form action="{{ url('post', [$post->pid]) }}" method="POST" >
                                                @csrf
                                                @method('DELETE')
                                                {{ csrf_field() }}
                                                <input type="hidden" name="path" value="post">
                                                <button class="dropdown-item" onclick="return confirm('Are you sure want to delete?');" type="submit"><i class="material-icons">delete</i>Delete</button>
                                            </form>
                                        
                                        </div>
                                </div>
                            @endif
                        <a href="{{ '/post/' . $post->pid }}" style="text-decoration:none; color:black;">     
                        <div>
                        <b>{{ $post->pname }}</b> 
                        </div>                  
                        <hr>              
                        <div>
                        <p class="detail">{{ $post->detail }}</p> 
                        </div>
                        </a>
                        <div>
                        <i class="fa fa-user-circle-o"></i> {{ $post->name }} &ensp;
                        <i class="fa fa-clock-o"></i>  {{$date[$i]->created_at}} 
						( แก้ไขล่าสุด  {{$date[$i]->updated_at}} ) &ensp;
                        <i class="fa fa-heart"></i> {{ $post->like }} &ensp;
                        </div>
                    </div>
                    </a>
                </div> 
            </div>
        </div>
        <?php $i++ ?>
        @endforeach
    </div>
@endsection
