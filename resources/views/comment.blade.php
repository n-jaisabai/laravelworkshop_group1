@section('title','List')

<?php $j=0 ?>
@foreach ($comment as $c)

<?php 
if (Auth::user()) {
	$user_id = Auth::user()->id;
} else {
	$user_id = -1;
}
?>

<div class="container">
	<div class="card">	<span>
						<div class="float-left" style="margin-left:2%; margin-top:1%;">
								@for ($i = 0;$i<$c->star; $i++)
								<span class="float-right"><i class="text-warning fa fa-star"></i></span>
								@endfor
						</div>
						@if ( $user_id == $post->id)
						<div class="dropdown show mt-2 mr-4 float-right">
								<a class="btn btn-warning dropdown-toggle btn-sm" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								  Rating
								</a>
								<form action="{{url('comment',[$c->cid])}}" method="POST">
								@csrf
								@method('PUT')
								<div class="dropdown-menu" >
								<input type="hidden" value="{{$c->detail}}" name="editdetail">
								<input type="hidden" value="{{$c->pid}}" name="pid">
								  <button class="dropdown-item" type="submit" name="star" value="1">1</button>
								  <button class="dropdown-item" type="submit" name="star" value="2">2</button>
								  <button class="dropdown-item" type="submit" name="star" value="3">3</button>
								  <button class="dropdown-item" type="submit" name="star" value="4">4</button>
								  <button class="dropdown-item" type="submit" name="star" value="5">5</button>
								</div>
								</form>
							  </div>
						@endif
						</span>
	    <div class="card-body">
	        <div class="row">
        	    <div class="col-md-1">
        	        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" style="width: 60px; height: 60px;"/>
				</div>
        	    <div class="col-md-11">
        	        <p>
        	            <strong>{{$c->name}}</strong></a>
        	       </p>
        	       <div class="clearfix"></div>
					<p>{{ $c->detail }}</p>
					<div class="row">
						<div class="col-md-9">
							<p class="text-secondary"><i class="fa fa-clock-o"></i> {{$c->created_at}} 
								( แก้ไขล่าสุด : {{$c->updated_at}} )
							</p>	
						</div>
						<div class="col-md-3">
							@if ( $user_id == $c->id) 
							<div style="text-align:right; margin:1%; float: right;" class="form-inline ">
									<form class="mr-2">
											<button type="button" data-toggle="collapse" data-target="#edit{{$c->cid}}" 
												class="btn btn-info btn-sm" style="color:#ffffff"><i class="fa fa-pen"> Edit</i></button>
									</form> 
									<form action="{{ url('comment', [$c->cid]) }}" method="POST" >
											@csrf
											@method('DELETE')
											{{ csrf_field() }}
											<input type="hidden" name="pid" value="{{ $post->pid }}">
											<button class="btn btn-danger btn-sm" class="dropdown-item" onclick="return confirm('Are you sure want to delete?');" type="submit"><i class="fas fa-trash-alt"> Delete</i></button>
									</form>
							</div>
							@endif
						</div>
					</div>			
				</div> 
			</div>
		</div>
		<div id="edit{{$c->cid}}" class="collapse" style="margin:2%">
				@include('editcomment') 
		</div>
	</div>
</div>
<br>
<?php $j++; ?>
@endforeach
