@extends('layouts/app')

@section('title', 'New Post')

@section('content')

<?php 
  if (Auth::user()) {
    $user_id = Auth::user()->id;
  } else {
    $user_id = -1;
  }
?>
<form action="{{ url('post') }}" method="POST">
    @csrf
    <div class="card text-center " >
    <div class="form-group">
        <div class="card-header border rounded border-success" style="background-color: rgb(0, 149, 122)" >
      <h3 for="title " class="card-title fontlogo text-light font-weight-bold">Title</h3>
        </div>
        <div class="card-body">
      <input type="text" class="form-control" id="title" name="title" placeholder="ชื่อกระทู้">
        </div>
    </div>

    <div class="form-group">
        <div class="card-header border border-5 rounded border-success" style="background-color: rgb(0, 149, 122)">
      <h3 for="description" class="card-title text-light font-weight-bold">Description</h3>
        </div>
        <div class="card-body">
      <textarea class="form-control" id="description" name="description" rows="5"></textarea>
        </div>
    </div>

  <input type="hidden" name="uid" value="{{ $user_id }} ">
  <div class="card-body ">
    <button type="submit" class="btn  btn-block fontlogo text-light font-weight-bold" style="background-color: rgb(0, 149, 122)">Submit</button>
  </div>

    @if ($errors->any())
        <div class="alert alert-danger mt-3">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
     @endif

  </form>
@endsection
