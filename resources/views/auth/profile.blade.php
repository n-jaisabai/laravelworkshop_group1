<style>
    .fb-profile img.fb-image-lg{
      z-index: 0;
      width: 100%;  
      margin-bottom: 10px;
  }

  .fb-image-profile
  {
      margin: -90px 10px 0px 50px;
      z-index: 9;
      width: 16%; 
  }

  @media (max-width:768px)
  {
      
  .fb-profile-text h1{
      font-weight: 700;
      font-size:16px;
  }

  .fb-image-profile
  {
      margin: -45px 10px 0px 25px;
      z-index: 9;
      width: 16%; 
  }
  #b{ pointer-events: none;}
}
</style>
@extends('layouts.app')
@section('title', $profile[0]->name)
@section('content')
<?php 
  if (Auth::user()) {
    $user_id = Auth::user()->id;
  } else {
    $user_id = -1;
  }
?>
<div class="card border-success mb-3">
    <div class="card-body text-success">
      <p class="card-text">
          <div class="container">
            <div class="fb-profile">
              <img align="left" class="fb-image-lg" height="50%" src="https://wallpaper.campus-star.com/app/uploads/2017/04/greentree02.jpg"/><input disabled="disabled" class="btn" style="height: 0px;width: 50%;" type="button" value="">
              <img align="left" class="fb-image-profile thumbnail" src="http://www.bkkedu.in.th/wp-content/uploads/2017/08/person_blank-223x298.png" alt="Profile image example"/>
              <div class="fb-profile-text">
              @foreach ($profile as $item)
                <h1>{{ $item->name }}</h1>
                <p>Email : {{ $item->email }}</p>
                <p>Membership date : {{ $item->created_at }}</p> 
              @endforeach
            </div>
          </div>
        </div> <!-- /container -->  
      </p>
    </div>
  </div>
</div> 
<div class="container">
  <?php $sum = 0;$l = 1?>
  @foreach ($mypost as $item)
    <?php $sum += 1;?>
  @endforeach
    <div class="row justify-content-center">
      <?php if($sum == -1){$sum = 0;}?>
      <div class="container ">
      <div class="card border-success mb-3">
        <div class="card-header bg-transparent border-success"><h2>My post</h2><h5>Total posts : <?=$sum?></h5></div>
        <div class="card-body text-success">
          <p class="card-text">
            <table class="table table-hover">
              <thead  class="bg-success">
                <tr>
                  <th>No.</th>
                  <th>Title</th>
                  <th>Time posted</th>
                  <th colspan="2">Detail</th>
                </tr>
              </thead>
              <tbody>
              @foreach ($mypost as $item)
                <tr>
                  <td><?=$l++?></td>
                  <td>{{$item->pname}}</td>
                  <td>{{$item->created_at}}</td>
                  <td class="form-inline ">
                        <a href="{{ '/post/' . $item->pid }}" class="mb-3">
                          <button type="button" class="btn btn-success" value=""><i class="fas fa-sliders-h"></i>DETAIL</button>
                        </a> 
                      @if ( $user_id == $profile[0]->id) 
                      <form action="{{ url('post', [$item->pid]) }}" method="POST" class="ml-2">
                        @csrf
                        @method('DELETE')
                          {{ csrf_field() }}
                          <input type="hidden" name="path" value="{{'mypost/' . $item->id}}">
                          <button type="submit" onclick="return confirm('Are you sure want to delete?');" class="btn btn-success"><i class="fas fa-trash"></i>Delete</button>
                        </form>
                      @endif
                    </td>
                </tr>
              @endforeach
              </tbody>
          </table>
          </p>
        </div>
        <div class="card-footer bg-transparent border-success"></div>
      </div>
    </div>
  </div>
</div>
@endsection
